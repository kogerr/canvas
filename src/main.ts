namespace Canvas {
    const init = (): void => {
        const webSocket = listen();
        const stage = new Stage();
        const game = new Game(stage, webSocket);

        const addFireBolt = (fireBolt: FireBolt) => webSocket.send(JSON.stringify({ newFireBolt: fireBolt }));

        game.start();
        const player = createPlayer({ x: config.startPosition.x, y: config.startPosition.y });
        listenForServerUpdates(webSocket, game, player.name);

        const sendPlayerInfo = (wizard: Wizard) => {
            if(webSocket.readyState === 1) {
                webSocket.send(JSON.stringify({ me: wizard }));
            }
        };

        const enterStage = (startPosition: Coordinate): void => {
            player.position.x = startPosition.x;
            player.position.y = startPosition.y;
            game.addWizard(player);
            webSocket.send(JSON.stringify({ entry: player }));
        };

        listenForKeys(player, sendPlayerInfo);
        listenForClicks(player, addFireBolt, enterStage);
    };

    window.onload = init;
}
