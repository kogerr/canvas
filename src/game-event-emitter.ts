namespace Canvas {
    export type GameEvent = 'firebolt' | 'death';

    export class GameEventEmitter {
        private readonly listeners = new Map<GameEvent, Array<(emitter: this) => void>>();

        listen(type: GameEvent, callback: (emitter: this) => void) {
            if (this.listeners.has(type)) {
                this.listeners.get(type)?.push(callback);
            } else {
                this.listeners.set(type, [ callback ]);
            }
        }

        emit(type: GameEvent) {
            if (this.listeners.has(type)) {
                this.listeners.get(type)?.forEach(callback => callback(this));
            }
        }
    }
}
