namespace Canvas {
    export type Colour = string | CanvasGradient | CanvasPattern;
}
