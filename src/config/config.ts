namespace Canvas {
    const arena = {
        width: 1920,
        height: 1000,
    };

    export const ratio = window.innerWidth / arena.width;

    export const config = {
        arena,
        fireBolt: {
            radius: 5,
            speed: 12,
            drawRadius: 5 * ratio,
        },
        player: {
            colour: 'blue',
            name: 'Player 1',
            speed: 12,
        },
        roboWizard: {
            name: 'RoboWizard',
            speed: 4,
        },
        wizard: {
            radius: 5,
            drawRadius: 5 * ratio,
        },
        startPosition: {
            x: 960,
            y: 500,
        }
    };
}
