namespace Canvas {
    export interface FireBolt {
        position: Coordinate;
        readonly movement: Coordinate;
        readonly owner: string;
    }
}
