namespace Canvas {
    export class Game {
        private fireBolts: FireBolt[] = [];
        private wizards: Wizard[] = [];

        constructor(private readonly stage: Stage, private readonly webSocket: WebSocket) {
        }

        start(): number {
            return requestAnimationFrame(() => this.playLoop());
        }

        addWizard(wizard: Wizard): void {
            if (!this.wizards.some((w) => w.name === wizard.name)) {
                this.wizards.push(wizard);
            }
        }

        private playLoop(): void {
            this.stage.clear();
            [...this.fireBolts, ...this.wizards].forEach(this.move);
            this.fireBolts.forEach((fireBolt: FireBolt) => drawFireBolt(this.stage.context, fireBolt));
            this.wizards.forEach((wizard: Wizard) => this.playWizardsRound(wizard));
            this.findRemovableObjects();
            requestAnimationFrame(() => this.playLoop());
        }

        private displayGameOver(winnerName: string): void {
            if (confirm(`GAME OVER\n\nTHE WINNER IS ${ winnerName }\n\nSTART NEW GAME?`)) {
                window.location.reload();
            }
        }

        private move(gameObject: FireBolt | Wizard) {
            gameObject.position.x += gameObject.movement.x;
            gameObject.position.y += gameObject.movement.y;
        }

        private findRemovableObjects() {
            const removableFireBolts: FireBolt[] = [];
            const kills: { killed: string, by: string }[] = [];

            this.fireBolts.filter(this.isOutOfArea)
                .forEach((fireBolt) => removableFireBolts.push(fireBolt));

            this.wizards.forEach((wizard) => {
                this.fireBolts.filter((fireBolt) => checkCollision(fireBolt, wizard))
                    .forEach((fireBolt) => {
                        kills.push({ killed: wizard.name, by: fireBolt.owner });
                        removableFireBolts.push(fireBolt)
                    });
            });

            removableFireBolts.forEach((fireBolt) => {
                this.fireBolts.splice(this.fireBolts.indexOf(fireBolt), 1);
            });

            kills.forEach((kill) => {
                this.webSocket.send(JSON.stringify(kill));
                this.wizards.splice(this.wizards.findIndex(wizard => wizard.name === kill.killed), 1);
            });
        };

        private playWizardsRound(wizard: Wizard) {
            const { x, y } = goAround(wizard.position);
            wizard.position.x = x;
            wizard.position.y = y;
            drawWizard(this.stage.context, wizard.colour, wizard.position);
        }

        private readonly isOutOfArea = (gameObject: FireBolt): boolean =>
            gameObject.position.x > config.arena.width || gameObject.position.x < 0
                || gameObject.position.y > config.arena.height || gameObject.position.y < 0;

        public handlePositionUpdate(wizard: Wizard) {
            const localWizard = this.wizards.find((w) => w.name === wizard.name);
            if (localWizard) {
                localWizard.position.x = wizard.position.x;
                localWizard.position.y = wizard.position.y;
                localWizard.movement.x = wizard.movement.x;
                localWizard.movement.y = wizard.movement.y;
            }
        }

        public addFireBolt(fireBolt: FireBolt) {
            this.fireBolts.push(fireBolt);
        }

        public removeWizard(name: string) {
            const killedIndex = this.wizards.findIndex(wizard => wizard.name === name);
            if (killedIndex >= 0) {
                this.wizards.splice(killedIndex, 1);
            }
        }

        public handleStatusUpdate(update: { wizards: Wizard[], fireBolts: FireBolt[]}) {
            update.wizards.forEach((wizard: Wizard) => this.wizards.push(wizard));
            this.fireBolts = update.fireBolts;
        }
    }
}
