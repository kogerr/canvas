namespace Canvas {
    export interface Wizard {
        movement: Coordinate;
        colour: Colour;
        position: Coordinate;
        name: string;
    }
}
