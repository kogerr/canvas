namespace Canvas {
    export const listen = () => {
        const webSocket = new WebSocket(getWebSocketUrl());

        webSocket.addEventListener('open', event => {
            console.log(event);
        });

        webSocket.addEventListener('error', event => {
            console.error(event);
        });

        return webSocket;
    };

    const getWebSocketUrl = () => location.protocol === 'http:' ? `ws://${window.location.hostname}:80` : `wss://${window.location.hostname}:443`
}
