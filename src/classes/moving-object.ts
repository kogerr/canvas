namespace Canvas {
    export interface MovingObject {
        position: Coordinate;
        movement: Coordinate;
    }
}
