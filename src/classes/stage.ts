namespace Canvas {
    export class Stage {
        readonly context: CanvasRenderingContext2D;
        private readonly $canvas: HTMLCanvasElement;

        constructor() {
            this.$canvas = document.getElementsByTagName('canvas')[0];
            this.$canvas.width = this.$canvas.offsetWidth;
            this.$canvas.setAttribute('height', (this.$canvas.offsetWidth / 192 * 100).toString(10));
            this.$canvas.height = this.$canvas.offsetHeight;
            this.context = this.$canvas.getContext('2d') as CanvasRenderingContext2D;
        }

        clear(): void {
            this.context.clearRect(0, 0, this.$canvas.width, this.$canvas.height);
        }
    }
}
