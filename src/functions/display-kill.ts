namespace Canvas {
    export const displayMessage = (message: string) => {
        const label = document.createElement('div');
        label.innerText = message;
        label.className = 'message';
        document.body.appendChild(label);
        setTimeout(() => label.style.opacity = '0', 1000);
        setTimeout(() => document.body.removeChild(label), 2000);
    };
}
