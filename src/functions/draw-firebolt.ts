namespace Canvas {
    export const drawFireBolt = (context: CanvasRenderingContext2D, firebolt: Canvas.FireBolt) => {
        const x = firebolt.position.x * ratio;
        const y = firebolt.position.y * ratio;
        const radius = Canvas.config.fireBolt.drawRadius;
        const radians = Math.atan2(firebolt.movement.x * -1, firebolt.movement.y);
        const body = new Path2D();
        body.arc(x, y, radius, radians, radians + Math.PI);
        body.lineTo(x - firebolt.movement.x * radius * 0.25, y - firebolt.movement.y * radius * 0.25);
        body.closePath();

        const radialGradient = context.createRadialGradient(x + firebolt.movement.x * radius * 0.05,
            y + firebolt.movement.y * radius * 0.05, radius * 0.2, x, y, radius);
        radialGradient.addColorStop(0, 'red');
        radialGradient.addColorStop(1, 'orange');

        context.fillStyle = radialGradient;
        context.globalAlpha = 0.5;
        context.fill(body);
    }
}
