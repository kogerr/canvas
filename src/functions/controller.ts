namespace Canvas {
    const PLAYER_SPEED = config.player.speed;
    const moveKeys = {
        a: false,
        d: false,
        s: false,
        w: false,
    };
    export let isAlive = false;

    export const listenForKeys = (player: Wizard, sendPlayerInfo: (w: Wizard) => void): void => {
        window.onkeydown = (event: KeyboardEvent): void => {
            if (isAlive) {
                moveOnKeyDown(event, player, sendPlayerInfo);
            }
        };
    };

    const moveOnKeyDown = (event: KeyboardEvent, player: Wizard, sendPlayerInfo: (w: Wizard) => void) => {
        const key = event.key.toLowerCase();

        if ((key === 'a' || key === 'arrowleft') && !moveKeys.a) {
            moveKeys.a = true;
            if (!player.movement.x) {
                player.movement.x = -1 * PLAYER_SPEED;
                sendPlayerInfo(player);
            }
            const listener = (keyup: KeyboardEvent) => {
                if (keyup.key.toLowerCase() === key) {
                    player.movement.x = moveKeys.d ? PLAYER_SPEED : 0;
                    moveKeys.a = false;
                    sendPlayerInfo(player);
                    window.removeEventListener('keyup', listener);
                }
            };
            window.addEventListener('keyup', listener);
        } else if ((key === 'd' || key === 'arrowright') && !moveKeys.d) {
            moveKeys.d = true;
            if (!player.movement.x) {
                player.movement.x = PLAYER_SPEED;
                sendPlayerInfo(player);
            }
            const listener = (keyup: KeyboardEvent) => {
                if (keyup.key.toLowerCase() === key) {
                    player.movement.x = moveKeys.a ? -1 * PLAYER_SPEED : 0;
                    moveKeys.d = false;
                    sendPlayerInfo(player);
                    window.removeEventListener('keyup', listener);
                }
            };
            window.addEventListener('keyup', listener);
        } else if ((key === 'w' || key === 'arrowup') && !moveKeys.w) {
            moveKeys.w = true;
            if (!player.movement.y) {
                player.movement.y = -1 * PLAYER_SPEED;
                sendPlayerInfo(player);
            }
            const listener = (keyup: KeyboardEvent) => {
                if (keyup.key.toLowerCase() === key) {
                    player.movement.y = moveKeys.s ? PLAYER_SPEED : 0;
                    moveKeys.w = false;
                    sendPlayerInfo(player);
                    window.removeEventListener('keyup', listener);
                }
            };
            window.addEventListener('keyup', listener);
        } else if ((key === 's' || key === 'arrowdown') && !moveKeys.s) {
            moveKeys.s = true;
            if (!player.movement.y) {
                player.movement.y = PLAYER_SPEED;
                sendPlayerInfo(player);
            }
            const listener = (keyup: KeyboardEvent) => {
                if (keyup.key.toLowerCase() === key) {
                    player.movement.y = moveKeys.w ? -1 * PLAYER_SPEED : 0;
                    moveKeys.s = false;
                    sendPlayerInfo(player);
                    window.removeEventListener('keyup', listener);
                }
            };
            window.addEventListener('keyup', listener);
        }
    };

    export const listenForClicks = (player: Wizard, addFireBolt: (fb: FireBolt) => void, enterStage: (coordinate: Coordinate) => void): void => {
        window.onclick = (mouseEvent: MouseEvent): void => {
            const target = { x: mouseEvent.clientX / ratio, y: mouseEvent.clientY / ratio };
            if (isAlive) {
                addFireBolt(createFireBolt(player, target));
            } else {
                isAlive = true;
                enterStage(target);
            }
        };
    };
}
