namespace Canvas {
    const speed = Canvas.config.fireBolt.speed;

    export const createFireBolt = (owner: Wizard, target: Coordinate): FireBolt => {
        const position = { x: owner.position.x, y: owner.position.y };
        const movement = calculateMovement(owner.position, target);
        return { position, movement, owner: owner.name };
    };

    const calculateMovement = (start: Coordinate, target: Coordinate): Coordinate => {
        const horizontalDifference = target.x - start.x;
        const verticalDifference = target.y - start.y;

        const distance = Math.sqrt(horizontalDifference * horizontalDifference + verticalDifference * verticalDifference);
        const ratio = speed / distance;

        const horizontalSpeed = horizontalDifference * ratio;
        const verticalSpeed = verticalDifference * ratio;

        return { x: horizontalSpeed, y: verticalSpeed };
    }
}
