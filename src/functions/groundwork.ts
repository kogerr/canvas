namespace Canvas {
    const PLAYER_NAME = config.player.name;
    const PLAYER_COLOUR = config.player.colour;
    const ROBO_WIZARD_SPEED = config.roboWizard.speed;
    const ROBO_WIZARD_NAME = config.roboWizard.name;

    export const createPlayer = (position: Coordinate): Wizard => {
        return { colour: PLAYER_COLOUR, position, name: (new Date()).getMilliseconds().toString(), movement: { x: 0, y: 0 } };
    };

    export const createRoboWizard = (position: Coordinate, addFireBolt: (fb: FireBolt) => void, target: Coordinate): Wizard => {
        const colour = createColour();
        const roboWizard = { colour, position, name: ROBO_WIZARD_NAME, movement: { x: 0, y: 0 } };

        setDirectionAlternation(roboWizard, ROBO_WIZARD_SPEED);
        setTimeout(() => setRoboWizardShoot(roboWizard, target, addFireBolt), 3000);

        return roboWizard;
    };

    const createColour = (): Colour => '#' + (Math.floor(Math.random() * 16777216)).toString(16).padStart(6, '0');

    const setDirectionAlternation = (roboWizard: Wizard, speed: number): void => {
        const horizontalSpeed = Math.random() * speed * 2 - speed;
        const verticalSpeed = Math.sqrt(speed * speed - horizontalSpeed * horizontalSpeed);
        const verticalDirection = Math.random() > 0.5 ? 1 : -1;
        roboWizard.movement = { x: horizontalSpeed, y: verticalSpeed * verticalDirection };
        setTimeout(() => setDirectionAlternation(roboWizard, speed), Math.random() * 1000);
    };

    const setRoboWizardShoot = (roboWizard: Wizard, target: Coordinate, addFireBolt: (fb: FireBolt) => void): void => {
        addFireBolt(createFireBolt(roboWizard, target));
        setTimeout(() => setRoboWizardShoot(roboWizard, target, addFireBolt), Math.random() * 1000);
    };
}
