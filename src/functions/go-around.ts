namespace Canvas {
    export const goAround = (position: Coordinate): Coordinate => {
        let { x, y } = position;

        if (x > config.arena.width) {
            x = 0;
        } else if (x < 0) {
            x = config.arena.width;
        }
        if (y > config.arena.height) {
            y = 0;
        } else if (y < 0) {
            y = config.arena.height;
        }

        return { x, y };
    };
}
