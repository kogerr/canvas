namespace Canvas {
    const radius = Canvas.config.fireBolt.radius;

    export const checkCollision = (fireBolt: FireBolt, wizard: Wizard) =>
        !isOwner(fireBolt, wizard) && isInRange(fireBolt, wizard);

    const isOwner = (fireBolt: FireBolt, wizard: Wizard) => fireBolt.owner === wizard.name;

    const isInRange = (fireBolt: FireBolt, wizard: Wizard): boolean =>
        wizard.position.x > fireBolt.position.x - radius
        && wizard.position.x < fireBolt.position.x + radius
        && wizard.position.y > fireBolt.position.y - radius
        && wizard.position.y < fireBolt.position.y + radius;
}
