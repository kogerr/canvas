namespace Canvas {
    export const drawWizard = (context: CanvasRenderingContext2D, colour: Colour, coordinate: Coordinate): void => {
        const x = coordinate.x * ratio;
        const y = coordinate.y * ratio;
        const radius = Canvas.config.wizard.drawRadius;
        const body = new Path2D();

        body.arc(x, y, radius, 0, 2 * Math.PI);
        body.closePath();
        context.fillStyle = colour;
        context.globalAlpha = 1;
        context.fill(body);
    };
}
