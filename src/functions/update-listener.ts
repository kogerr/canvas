namespace Canvas {
    export const listenForServerUpdates = (webSocket: WebSocket, game: Game, playerName: string) => {
        webSocket.addEventListener('message', event => {
            const update = JSON.parse(event.data);
            if ('wizard' in update) {
                game.handlePositionUpdate(update.wizard);
            } else if('newFireBolt' in update) {
                game.addFireBolt(update.newFireBolt);
            } else if ('killed' in update) {
                if (update.killed === playerName && isAlive) {
                    isAlive = false;
                }
                displayMessage(`${update.by} shot ${update.killed}`);
                game.removeWizard(update.killed);
            } else if ('entry' in update) {
                game.addWizard(update.entry);
                displayMessage(`${update.entry.name} entered the game`);
            } else if ('wizards' in update) {
                game.handleStatusUpdate(update);
            }
        });
    }
}
