FROM node:10

WORKDIR /usr/src/app

COPY *.json ./
RUN npm install
COPY server ./server
COPY src ./src
COPY dist ./dist

RUN npm run build

EXPOSE 80
EXPOSE 443

WORKDIR /usr/src/app/dist/server

ENTRYPOINT [ "node", "app.js" ]
