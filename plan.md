* Kill info
  * Tell server about killer
  * Broadcast death once
  * Remove player on both sides

* Out of game state - observer mode
  * click-start
  * died message

* Scoreboard
  * kill point popup
  * broadcast scoreboard on kills
  * store scoreboard on client side - popup

* State of active connections
  * Store on server-side by IP or session or sg.
  * unique colours
  * remove on disconnect

* Game start
  * registration
  * click-start
