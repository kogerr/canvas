import { initServer } from './server';
import { Server } from 'http';
import { initWebsocket } from './websocket';

initServer().then((server: Server) => initWebsocket(server));
