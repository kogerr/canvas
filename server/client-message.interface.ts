import { FireBolt } from './fire-bolt';
import { Wizard } from './wizard';

export interface KillRequest {
    killed: string
    by: string
}

export interface EntryRequest {
    entry: Wizard
}

export interface PositionRequest {
    me: Wizard
}

export interface FireBoltRequest {
    newFireBolt: FireBolt
}
