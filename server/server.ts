import { Credentials } from './credentials';
import { RequestHandler } from './types';
import { createServer, ServerOptions } from 'http';
import { SecureServerOptions } from 'http2';
import { createServer as createHttpsServer } from 'https';
import { readCredentials } from './read-credentials';
import { route } from './routing';

const httpPort = 80;
const httpsPort = 443;
const redirectStatusCode = 301;

let credentials: Credentials | undefined;

const listenOnHttps = (requestListener: RequestHandler, options: SecureServerOptions) => {
    const server = createHttpsServer(options, requestListener);
    server.listen(httpsPort, () => {
        console.log(`Listening on port ${httpsPort}`);
    });
    server.on('error', console.error);

    return server;
};

const listenOnHttp = (requestListener: RequestHandler, options: ServerOptions = {}) => {
    const server = createServer(options, requestListener);
    server.listen(httpPort, () => {
        console.log(`Listening on port ${httpPort}`);
    });
    server.on('error', console.error);

    return server;
};

export const getServer = (loadedCredentials: Credentials | undefined) => (requestHandler: RequestHandler) => {
    let httpRequestHandler: RequestHandler;
    credentials = loadedCredentials;
    let httpsServer;

    if (credentials !== undefined) {
        httpRequestHandler = httpsRedirect;
        httpsServer = listenOnHttps(requestHandler, credentials);
    } else {
        httpRequestHandler = requestHandler;
    }

    const httpServer = listenOnHttp(httpRequestHandler);

    return httpsServer ? httpsServer : httpServer;
};

export const initServer = () => readCredentials().then(getServer).then((server) => server(route));

export const getCredentials = () => credentials;

const httpsRedirect: RequestHandler = (req, res) => {
    res.writeHead(redirectStatusCode, { Location: 'https://' + req.headers.host + req.url });
    res.end();
};
