import { Credentials } from './credentials';
import { readFileContent } from './file-reader';

const certPath = process.env.CERT_PATH || 'cert';

export const readCredentials = (): Promise<Credentials | undefined> => new Promise((resolve) => {
    const keyPromise = readFileContent(certPath + '/privkey.pem');
    const certPromise = readFileContent(certPath + '/cert.pem');
    const caPromise = readFileContent(certPath + '/chain.pem');

    Promise.all([keyPromise, certPromise, caPromise])
        .then(([key, cert, ca]) => resolve({ key: key.toString(), cert: cert.toString(), ca: ca.toString() } as Credentials))
        .catch(() => resolve(undefined));
});
