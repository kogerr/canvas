import { Coordinate } from './coordinate';

export interface Wizard {
    movement: Coordinate;
    colour: string;
    position: Coordinate;
    name: string;
}
