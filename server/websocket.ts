import { Server } from 'http';
import * as ws from 'websocket';
import { getWizards, handleMessage } from './message-handler';

export const initWebsocket = (httpServer: Server) => {
    // @ts-ignore
    const wsServer = new ws.server({httpServer});

    // @ts-ignore
    wsServer.on('request', (request: ws.request) => {
        const connection = request.accept(undefined, request.origin);
        console.log('connected host:', request.remoteAddress);
        connection.send(JSON.stringify(getWizards()));

        // @ts-ignore
        connection.on('message', (message: ws.IMessage) => {
            if (message.utf8Data) {
                handleMessage(message.utf8Data, (data: string) => wsServer.broadcastUTF(data));
            }
        });

        connection.on('close', (code: number) => {
            console.log('Connection closed with ' + code);
        });
    });
};
