export interface NamedFile {
    path: string;
    name: string;
    content: Buffer;
}
