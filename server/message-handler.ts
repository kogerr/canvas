import { Wizard } from './wizard';
import { FireBolt } from './fire-bolt';
import { ClientRequest } from './types';

const wizards: Wizard[] = [];
const fireBolts: FireBolt[] = [];
const deadWizards: Wizard[] = [];

const arena = {
    width: 1920,
    height: 1000,
};
const radius = 5;

export const handleMessage = (rawMessage: string, send: (update: string) => void) => {
    const message = JSON.parse(rawMessage) as ClientRequest;
    if ('newFireBolt' in message) {
        send(rawMessage);
    } else if ('me' in message) {
        const user = message.me;
        const localWizard = wizards.find((w) => w.name === user.name);
        if (localWizard) {
            localWizard.position.x = user.position.x;
            localWizard.position.y = user.position.y;
            localWizard.movement.x = user.movement.x;
            localWizard.movement.y = user.movement.y;
        }
        send(JSON.stringify({ wizard: user }));
    } else if ('killed' in message) {
        const deadIndex = wizards.findIndex((w) => w.name === message.killed);
        if (deadIndex >= 0) {
            wizards.splice(deadIndex, 1);
            send(rawMessage);
        }
    } else if ('entry' in message) {
        wizards.push(message.entry);
        send(rawMessage);
    }
};

export const getWizards = () => ({ wizards, fireBolts });

const findRemovableObjects = () => {
    const removableFireBolts: FireBolt[] = [];
    const removableWizards: Wizard[] = [];

    fireBolts.filter(isOutOfArea)
        .forEach((fireBolt) => removableFireBolts.push(fireBolt));

    fireBolts.forEach((fireBolt) => {
        wizards.filter(wizard => checkCollision(fireBolt, wizard))
            .forEach((wizard) => {
                removableWizards.push(wizard);
                removableFireBolts.push(fireBolt)
            });
    });

    removableFireBolts.forEach((fireBolt) => {
        fireBolts.splice(fireBolts.indexOf(fireBolt), 1);
    });

    removableWizards.forEach((wizard) => {
        wizards.splice(wizards.indexOf(wizard), 1);
        deadWizards.push(wizard);
    });
};

const isOutOfArea = (gameObject: FireBolt): boolean =>
    gameObject.position.x > arena.width || gameObject.position.x < 0
    || gameObject.position.y > arena.height || gameObject.position.y < 0;

const checkCollision = (fireBolt: FireBolt, wizard: Wizard) =>
    !isOwner(fireBolt, wizard) && isInRange(fireBolt, wizard);

const isOwner = (fireBolt: FireBolt, wizard: Wizard) => fireBolt.owner === wizard.name;

const isInRange = (fireBolt: FireBolt, wizard: Wizard): boolean =>
    wizard.position.x > fireBolt.position.x - radius
    && wizard.position.x < fireBolt.position.x + radius
    && wizard.position.y > fireBolt.position.y - radius
    && wizard.position.y < fireBolt.position.y + radius;
