import { RequestHandler } from './types';
import { ServerResponse } from 'http';
import { Http2ServerResponse } from 'http2';
import { fileExists, readFileContent } from './file-reader';
import { mimeTypes } from './mime-types';

const staticDirectory = '../src/';
const page404 = '<h1>Page not found</h1>';

export const route: RequestHandler = (req, res) => {
    const path = (!req.url || req.url === '/') ? '/index.html' : req.url;
    const method = req.method ? req.method : 'GET';

    res.statusCode = 200;

    if (method === 'GET' && fileExists(staticDirectory + path)) {
        return sendFile(res, path);
    } else {
        res.statusCode = 404;
        res.end(page404);
    }
};

const sendFile = (res: ServerResponse | Http2ServerResponse, path: string): Promise<void> => {
    const type = mimeTypes.get(path.slice(path.lastIndexOf('.')));
    if (type) {
        res.setHeader('Content-Type', type);
    }

    return readFileContent(staticDirectory + path).then((content: Buffer) => {
        res.statusCode = 200;
        res.end(content, 'binary');
    }).catch((error) => {
        res.statusCode = 500;
        res.end(JSON.stringify({ error }));
    });
};
