import { IncomingMessage, ServerResponse } from 'http';
import { Http2ServerRequest, Http2ServerResponse } from 'http2';
import { EntryRequest, FireBoltRequest, KillRequest, PositionRequest } from './client-message.interface';

export type RequestHandler = (req: IncomingMessage | Http2ServerRequest, res: ServerResponse | Http2ServerResponse) => void;
export type ClientRequest = EntryRequest | FireBoltRequest | KillRequest | PositionRequest
