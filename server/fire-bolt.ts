import { Coordinate } from './coordinate';

export interface FireBolt {
    position: Coordinate;
    readonly movement: Coordinate;
    readonly owner: string;
}
